import { consultaCEP } from '../../src/sigep-web';
import { env } from '../../src/main/config/env';

describe('sigep-web (busca-cep)', () => {
  test('Should return a true address', async () => {
    const ret = await consultaCEP(
      '15061719',
      env.SIGEP_USUARIO as string,
      env.SIGEP_SENHA as string,
    );
    expect(ret.bairro).toBeTruthy();
    expect(ret.cidade).toBeTruthy();
    expect(ret.end).toBeTruthy();
    expect(ret.uf).toBeTruthy();
  });

  test('Should return a error', async () => {
    try {
      await consultaCEP('wrong_cep', '', '');
    } catch (e) {
      expect(e).toBeInstanceOf(Error);
      expect(e.message).toBeTruthy();
    }
  });
});
