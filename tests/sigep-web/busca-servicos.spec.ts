import { env } from '../../src/main/config/env';
import { buscaServico } from '../../src/sigep-web';
import { BuscaServicoDTO } from '../../src/sigep-web/dto';

describe('sigep-web (busca-servicos)', () => {
  const request: BuscaServicoDTO = {
    idContrato: env.CONTRATO as string,
    idCartaoPostagem: env.CARTAO as string,
    usuario: env.SIGEP_USUARIO as string,
    senha: env.SIGEP_SENHA as string,
  };
  test('Should return a all services', async () => {
    const ret = await buscaServico(request);
    expect(Array.isArray(ret)).toBe(true);
  });
  test('Should return a error', async () => {
    try {
      await buscaServico({ ...request, idContrato: '99999999' });
    } catch (e) {
      expect(e).toBeInstanceOf(Error);
    }
  });
});
