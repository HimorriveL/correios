import { buscaCliente } from '../../src/sigep-web';
import { BuscaClienteDTO } from '../../src/sigep-web/dto';
import { env } from '../../src/main/config/env';

describe('sigep-web (busca-cliente)', () => {
  const request: BuscaClienteDTO = {
    idContrato: env.CONTRATO as string,
    idCartaoPostagem: env.CARTAO as string,
    usuario: env.SIGEP_USUARIO as string,
    senha: env.SIGEP_SENHA as string,
  };

  test('Should return a client', async () => {
    const ret = await buscaCliente(request);
    expect(ret.cnpj).toBeTruthy();
    expect(ret.id).toBeTruthy();
    expect(Array.isArray(ret.contratos)).toBe(true);
  });

  test('Should return a error', async () => {
    try {
      await buscaCliente({ ...request, idContrato: '99999999' });
    } catch (e) {
      expect(e).toBeInstanceOf(Error);
    }
  });
});
