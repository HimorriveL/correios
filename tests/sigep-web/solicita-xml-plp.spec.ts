import { solicitaXmlPLP } from '../../src/sigep-web';
import { SolicitaXmlPlpDTO } from '../../src/sigep-web/dto';
import { env } from '../../src/main/config/env';

describe('sigep-web (solicita-xml-plp)', () => {
  const request: SolicitaXmlPlpDTO = {
    idPlpMaster: env.NUMERO_PLP as string,
    usuario: env.SIGEP_USUARIO as string,
    senha: env.SIGEP_SENHA as string,
  };

  test('Should return a ICorreios PLP', async () => {
    const ret = await solicitaXmlPLP(request);
    expect(ret.tipo_arquivo).toBeTruthy();
    expect(ret.versao_arquivo).toBeTruthy();
    expect(ret.plp).toBeTruthy();
    expect(ret.plp.id_plp).toBeTruthy();
    expect(ret.remetente).toBeTruthy();
    expect(Array.isArray(ret.objeto_postal)).toBe(true);
  });

  test('Should return a error', async () => {
    try {
      await solicitaXmlPLP({ ...request, idPlpMaster: '99999999' });
    } catch (e) {
      expect(e).toBeInstanceOf(Error);
    }
  });
});
