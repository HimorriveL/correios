import { env } from '../../src/main/config/env';
import { buscaServico, solicitaEtiquetas } from '../../src/sigep-web';
import { BuscaServicoDTO, SolicitaEtiquetasDTO } from '../../src/sigep-web/dto';

describe('sigep-web (solicita-etiqueta)', () => {
  let listaServico = [];

  const requestServico: BuscaServicoDTO = {
    idContrato: env.CONTRATO,
    idCartaoPostagem: env.CARTAO,
    usuario: env.SIGEP_USUARIO,
    senha: env.SIGEP_SENHA,
  };

  let requestEtiquetas: SolicitaEtiquetasDTO;

  beforeAll(async () => {
    listaServico = await buscaServico(requestServico);

    requestEtiquetas = {
      idServico: listaServico[0].id,
      usuario: env.SIGEP_USUARIO,
      senha: env.SIGEP_SENHA,
      cnpj: env.CNPJ,
      quantidade: 1,
    };
  });

  test('Should return a etiqueta', async () => {
    const ret = await solicitaEtiquetas(requestEtiquetas);
    expect(ret.prefixo).toBeTruthy();
    expect(ret.sufixo).toBeTruthy();
    expect(ret.etiqueta_inicial).toBeTruthy();
    expect(ret.etiqueta_final).toBeTruthy();
  });

  test('Should return a error', async () => {
    try {
      await solicitaEtiquetas({ ...requestEtiquetas, cnpj: '999999999' });
    } catch (e) {
      expect(e).toBeInstanceOf(Error);
    }
  });
});
