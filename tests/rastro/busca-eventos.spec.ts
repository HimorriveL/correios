import { buscaEventos } from '../../src/rastro';
import { BuscaEventosDTO } from '../../src/rastro/dto';
import { env } from '../../src/main/config/env';

const request: BuscaEventosDTO = {
  usuario: env.RASTRO_USER as string,
  senha: env.RASTRO_SENHA as string,
  objetos: env.ETIQUETA as string,
};

describe('rastro (busca-eventos)', () => {
  test('Should return a true busca-eventos', async () => {
    const response = await buscaEventos(request);
    expect(response.versao).toBe('2.0');
    expect(response.qtd).toBe('1');
    expect(response.objeto).toBeInstanceOf(Array);
  });

  test('Should return a error', async () => {
    const response = await buscaEventos({
      ...request,
      usuario: 'WRONG_USER',
    });

    expect(response.objeto[0].numero).toBe('Erro');
    expect(response.objeto[0].erro).toBe('Usuário e/ou senha inválidos.');
    expect(response.objeto[0].evento.length).toBe(0);
    expect(response.objeto[0].evento[0]).toBe(undefined);
  });
});
