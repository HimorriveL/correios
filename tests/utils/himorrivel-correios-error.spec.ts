import { HimorrivelCorreiosError } from '../../src/utils';

describe('utils (himorrivel-correios-error)', () => {
  const error = new HimorrivelCorreiosError('Erro');

  test('Should return a error', () => {
    expect(error.message).toBe('Erro');
    expect(error).toBeInstanceOf(Error);
  });
});
