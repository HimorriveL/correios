export class CWSError extends Error {
  code: number;
  data: any;

  constructor(code: number, data: any) {
    super('Sua solicitação deu erro');
    this.code = code;
    this.data = data;
  }
}
