import { AxiosError } from 'axios';
import { parseStringPromise } from 'xml2js';
import { HttpResponse } from '../helpers';

async function sigepWebError(err: AxiosError): Promise<HttpResponse> {
  const options = {
    explicitArray: false,
  };

  const pre = await parseStringPromise(err.response?.data as any, options);
  return {
    data: {
      error: pre['soap:Envelope']['soap:Body']['soap:Fault'].faultstring,
    },
    statusCode: err.response?.status || 500,
  };
}

export { sigepWebError };
