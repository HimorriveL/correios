export interface HttpRequestClient {
  request: <T = any>(input: HttpRequestClient.Params) => Promise<T>;
}

export namespace HttpRequestClient {
  export type Params = {
    url: string;
    method: 'POST' | 'GET';
    body?: any;
    headers?: any;
    query?: any;
    auth?: any;
  };
}
