import axios, { AxiosError } from 'axios';
import { CWSError } from '../../application/errors';
import { HttpRequestClient } from './client';

export class AxiosHttpClient implements HttpRequestClient {
  async request({
    url,
    method,
    body,
    headers,
    query,
    auth,
  }: HttpRequestClient.Params): Promise<any> {
    try {
      const response = await axios({
        method,
        url,
        responseType: 'json',
        data: body,
        headers,
        params: query,
        auth,
      });

      return response.data;
    } catch (err) {
      console.log(err);
      if (err as AxiosError) {
        throw new CWSError(
          (err as AxiosError).response.status,
          (err as AxiosError).response.data,
        );
      }
      throw err;
    }
  }
}
