/* eslint-disable no-plusplus */
/* eslint-disable no-underscore-dangle */

import { cws } from '../..';
import { env } from '../../main/config/env';

async function solicita(): Promise<void> {
  try {
    const loginContrato = await cws.autentica.autenticaContrato({
      numero_contrato: env.AGF_CONTRATO,
      id_correios: env.AGF_ID,
      token_api: env.AGF_TOKEN_API,
    });

    console.log(loginContrato);

    const resp = await cws.pmaFornecedor.solicitaPostada({
      tokenContrato: loginContrato.token,
      codigoObjeto: 'BR994834796BR',
    });

    console.log(resp);
  } catch (e) {
    console.log(e);
  }
}

// eslint-disable-next-line no-void
void solicita();
