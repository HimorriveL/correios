class HimorrivelCorreiosError extends Error {
  constructor(message: string) {
    super(message);
    this.name = 'HimorrivelCorreiosError';
  }
}
export { HimorrivelCorreiosError };
