require('dotenv/config');

export const env = {
  URL_CALC_PRACO_PRAZO:
    'http://ws.correios.com.br/calculador/CalcPrecoPrazo.asmx?wsdl',
  URL_SIGEP_WEB:
    'https://apps.correios.com.br/SigepMasterJPA/AtendeClienteService/AtendeCliente?wsdl',
  URL_RASTRO: 'https://webservice.correios.com.br/service/rastro',

  SIGEP_USUARIO: process.env.SIGEP_USUARIO,
  SIGEP_SENHA: process.env.SIGEP_SENHA,
  CONTRATO: process.env.CONTRATO,
  CARTAO: process.env.CARTAO,
  CODIGO_ADMINISTRATIVO: process.env.CODIGO_ADMINISTRATIVO,
  CNPJ: process.env.CNPJ,
  DIGITOS_8_CNPJ: process.env.DIGITOS_8_CNPJ,
  CODIGO_SERVICO: process.env.CODIGO_SERVICO,
  RASTRO_USER: process.env.RASTRO_USER,
  RASTRO_SENHA: process.env.RASTRO_SENHA,
  ETIQUETA: process.env.ETIQUETA,
  NUMERO_PLP: process.env.NUMERO_PLP,
  ID_CORREIOS: process.env.ID_CORREIOS,
  TOKEN_API: process.env.TOKEN_API,
  AGF_TOKEN_API: process.env.AGF_TOKEN_API,
  AGF_CONTRATO: process.env.AGF_CONTRATO,
  AGF_ID: process.env.AGF_ID,
};
