export interface Preco {
  preco: (input: Preco.Params) => Promise<Preco.Output>;
}

export namespace Preco {
  export type Params = {
    tokenContrato: string;
    coProduto: string;
    cepOrigem: string;
    cepDestino: string;
    psObjeto: string;
    tpObjeto: string;
    comprimento: string;
    largura: string;
    altura: string;
    servicosAdicionais: string[];
    vlDeclarado: string;
  };
  export type Output = {
    coProduto: string;
    pcBase: string;
    pcBaseGeral: string;
    peVariacao: string;
    pcReferencia: string;
    vlBaseCalculoImposto: string;
    inPesoCubico: 'N' | 'S';
    psCobrado: string;
    peAdValorem: string;
    vlSeguroAutomatico: string;
    qtAdicional: string;
    pcFaixa: string;
    pcFaixaVariacao: string;
    pcProduto: string;
    pcFinal: string;
  };
}

export interface PrecoLista {
  precoLista: (input: PrecoLista.Params) => Promise<PrecoLista.Output>;
}

export namespace PrecoLista {
  type ParametrosServAdicional = {
    coServAdicional: string;
    tpServAdicional?: string;
    pcServicoAdicional?: string;
  };

  type ParametrosProduto = {
    coProduto: string;
    nuRequisicao: string;
    cepOrigem: string;
    cepDestino: string;
    psObjeto: string;
    tpObjeto: string;
    comprimento: string;
    largura: string;
    altura: string;
    servicosAdicionais: ParametrosServAdicional[];
    vlDeclarado: string;
    dtEvento: string;
    psCubico: string;
  };
  export type Params = {
    tokenContrato: string;
    parametrosProduto: ParametrosProduto[];
  };
  export type Output = {
    coProduto: string;
    pcBase: string;
    pcBaseGeral: string;
    peVariacao: string;
    pcReferencia: string;
    vlBaseCalculoImposto: string;
    nuRequisicao: string;
    inPesoCubico: 'N' | 'S';
    psCobrado: string;
    servicoAdicional: ParametrosServAdicional[];
    peAdValorem: string;
    vlSeguroAutomatico: string;
    qtAdicional: string;
    pcFaixa: string;
    pcFaixaVariacao: string;
    pcProduto: string;
    pcTotalServicosAdicionais: string;
    pcFinal: string;
  }[];
}

export interface ServicosAdicionais {
  servicosAdicionais: (
    input: ServicosAdicionais.Params,
  ) => Promise<ServicosAdicionais.Output>;
}

export namespace ServicosAdicionais {
  type Excludente = {
    coExcludente: string;
    dtIniVigencia: string;
    dtFimVigencia: string;
  };

  type Requeridos = Excludente & {
    coSubstituto: string;
  };

  export type Params = {
    tokenContrato: string;
    coProduto: string;
  };
  export type Output = {
    codigo: string;
    nome: string;
    sigla: string;
    sgPais: string;
    preco: number;
    tipo: string;
    coIndicador: string;
    noIndicador: string;
    sgIndicador: string;
    vlMinDeclarado: number;
    vlMaxDeclarado: number;
    mensagem: string;
    requeridos: Requeridos[];
    excludentes: Excludente[];
    dtIniVigencia: string;
    dtFimVigencia: string;
  }[];
}
