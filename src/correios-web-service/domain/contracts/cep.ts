export interface Cep {
  cep: (input: Cep.Params) => Promise<Cep.Output>;
}

export namespace Cep {
  export type Params = {
    cep: string;
    tokenContrato: string;
  };
  export type Output = {
    cep: string;
    uf: string;
    numeroLocalidade: number;
    localidade: string;
    logradouro: string;
    tipoLogradouro: string;
    nomeLogradouro: string;
    abreviatura: string;
    bairro: string;
    tipoCEP: number;
    cepUnidadeOperacional: string;
  };
}
