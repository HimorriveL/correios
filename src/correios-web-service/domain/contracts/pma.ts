type IEndereco = {
  cep: string;
  logradouro: string;
  numero: string;
  complemento?: string;
  bairro: string;
  cidade: string;
  uf: string;
  pais?: string;
};

type IPessoa = {
  nome: string;
  dddTelefone?: string;
  telefone?: string;
  dddCelular?: string;
  celular?: string;
  email?: string;
  cpfCnpj: string;
  documentoEstrangeiro?: string;
  obs?: string;
  endereco: IEndereco;
};

type IDeclaracaoConteudo = {
  conteudo: string;
  quantidade: number;
  valor: number;
};

type IServicoAdicional = {
  codigoServicoAdicional: string;
  tipoServicoAdicional?: string;
  nomeServicoAdicional?: string;
  valorServicoAdicional?: string;
  valorDeclarado?: number;
  siglaServicoAdicional?: string;
  orientacaoEntregaVizinho?: string;
  tipoChecklist?: string;
  subitensCheckList?: [
    {
      codigo: string;
    },
  ];
};

type IPrePostagem = {
  idCorreios?: string;
  remetente: IPessoa;
  destinatario: IPessoa;
  codigoServico: string;
  codigoObjeto?: string;
  pesoInformado: string;
  codigoFormatoObjetoInformado: string;
  alturaInformada: string;
  larguraInformada: string;
  comprimentoInformado: string;
  diametroInformado: string;
  cienteObjetoNaoProibido: number;
  numeroNotaFiscal?: string;
  chaveNFe?: string;
  itensDeclaracaoConteudo?: IDeclaracaoConteudo[];
  listaServicoAdicional?: IServicoAdicional[];
  ncmObjeto?: string;
  solicitarColeta?: string;
  nuColeta?: string;
  rfidObjeto?: string;
  sequencial?: string;
  precoServico?: string;
  precoPrePostagem?: string;
  numeroCartaoPostagem?: string;
  idAtendimento?: string;
  dataPrevistaPostagem?: string;
  observacao?: string;
  modalidadePagamento?: string;
  logisticaReversa?: 'S' | 'N';
  dataValidadeLogReversa?: string;
};

export interface RangeEtiqueta {
  solicitaRangeEtiqueta: (
    input: RangeEtiqueta.Params,
  ) => Promise<RangeEtiqueta.Output>;
}

export namespace RangeEtiqueta {
  export type Params = {
    codigoServico: string;
    quantidade: number;
    tokenCartao: string;
  };
  export type Output = {
    tipoPostal: string;
    nuEtiquetaInicial: string;
    nuEtiquetaFinal: string;
    numeroContrato: string;
    numeroCartaoPostagem: string;
    dataHoraSolicitacao: string;
  }[];
}

export interface PrePostagem {
  solicitaPrePostagem: (
    input: PrePostagem.Params,
  ) => Promise<PrePostagem.Output>;
}

export namespace PrePostagem {
  export type Params = {
    tokenCartao: string;
    numeroCartaoPostagem: string;
    idCorreios: string;
    body: IPrePostagem;
  };
  export type Output = {
    id: string;
    idCorreios: string;
    numeroCartaoPostagem: string;
    codigoObjeto: string;
    codigoServico: string;
    modalidadePagamento: 2;
    numeroNotaFiscal: string;
    pesoInformado: string;
    codigoFormatoObjetoInformado: string;
    alturaInformada: string;
    larguraInformada: string;
    comprimentoInformado: string;
    diametroInformado: string;
    cienteObjetoNaoProibido: 1;
    logisticaReversa: string;
    statusAtual: 2;
    dataHoraStatusAtual: string;
    descStatusAtual: string;
    dataHora: string;
    tipoRotulo: string;
    sistemaOrigem: string;
    tipoObjeto: string;
    prazoPostagem: string;
    objetoCargo: string;
  };
}

export interface PrePostagemLista {
  solicitaPrePostagemLista: (
    input: PrePostagemLista.Params,
  ) => Promise<PrePostagemLista.Output>;
}

export namespace PrePostagemLista {
  export type Params = {
    numeroCartaoPostagem: string;
    idCorreios: string;
    tokenCartao: string;
    listaPostagem: IPrePostagem[];
  };
  export type Output = {
    idLote: string;
    dataInclusao: string;
    statusProcessamento: string;
    nomeArquivoLote: string;
    tipoLote: string;
    numeroCartaoPostagem: string;
    erroProcessamento: boolean;
  };
}
