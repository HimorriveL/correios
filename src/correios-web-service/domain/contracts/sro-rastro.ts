export interface Objetos {
  objetos: (input: Objetos.Params) => Promise<Objetos.Output>;
}

export namespace Objetos {
  export type Params = {
    codigosObjetos: string[];
    resultado: 'T' | 'P' | 'U';
    tokenContrato: string;
  };
  export type Output = {
    versao: string;
    quantidade: number;
    objetos: Objeto[];
    tipoResultado: string;
  };
}

export interface RastrosDocumento {
  rastrosDocumento: (
    input: RastrosDocumento.Params,
  ) => Promise<RastrosDocumento.Output>;
}

export namespace RastrosDocumento {
  export type Params = {
    documento?: string;
    email?: string;
    celular?: string;
    resultado?: 'T' | 'P' | 'U';
    destinatarioRemetente?: 'A' | 'D' | 'R';
    tokenContrato: string;
  };
  export type Output = Objetos.Output;
}

export interface ObjetoImagens {
  objetoImagens: (input: ObjetoImagens.Params) => Promise<ObjetoImagens.Output>;
}

export namespace ObjetoImagens {
  export type Params = {
    listaEtiqueta: string[];
    tokenCartao: string;
  };
  export type Output = {
    user: string;
    numero: string;
    dtCriacao: Date;
    dtValidade: Date;
    idioma: string;
  };
}

export interface Recibo {
  recibo: (input: Recibo.Params) => Promise<Recibo.Output>;
}

export namespace Recibo {
  export type Params = {
    recibo: string;
    tokenContrato: string;
  };
  export type Output = [
    {
      objeto: string;
      observacao: string;
      imagens: [
        {
          latitude: string;
          longitude: string;
          imagem: string;
          comentario: string;
        },
      ];
    },
  ];
}

type TipoPostal = {
  sigla: string;
  descricao: string;
  categoria: string;
};

type Recebedor = {
  nome: string;
  documento: string;
  celular: string;
  email: string;
  comentario: string;
};

type Endereco = {
  cep: string;
  logradouro: string;
  complemento: string;
  numero: string;
  bairro: string;
  cidade: string;
  uf: string;
  pais: string;
  telefone: string;
};

type Unidade = {
  nome: string;
  codSro: string;
  codMcu: string;
  tipo: string;
  endereco: Endereco;
  codSe: string;
  se: string;
};

type Destino = {
  nome: string;
  codSro: string;
  codMcu: string;
  tipo: string;
  endereco: Endereco;
  codSe: string;
  se: string;
};

type EntregadorExterno = {
  documento: string;
  nome: string;
};

type Telefone = {
  tipo: string;
  ddd: string;
  numero: string;
};

type Remetente = {
  nome: string;
  documento: string;
  endereco: Endereco;
  telefones: Telefone[];
};

type Destinatario = {
  nome: string;
  documento: string;
  endereco: Endereco;
  telefones: Telefone[];
};

type Evento = {
  codigo: string;
  tipo: string;
  dtHrCriado: Date;
  descricao: string;
  estacao: string;
  usuario: string;
  entregador: string;
  latitude: string;
  longitude: string;
  detalhe: string;
  descCodigo: string;
  dtHrGravado: Date;
  recebedor: Recebedor;
  unidade: Unidade;
  entregadorExterno: EntregadorExterno;
  remetente: Remetente;
  destinatario: Destinatario;
  dtLimiteRetirada: Date;
  comentario: string;
  codLista: string;
  unidadeDestino: Destino;
};

type Objeto = {
  mensagem?: string;
  codObjeto: string;
  tipoPostal: TipoPostal;
  dtPrevista: Date;
  largura: number;
  comprimento: number;
  altura: number;
  peso: number;
  formato: string;
  modalidade: string;
  valorDeclarado: number;
  eventos: Evento[];
};
