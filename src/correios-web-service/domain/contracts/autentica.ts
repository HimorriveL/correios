export interface Autentica {
  autentica: (input: Autentica.Params) => Promise<Autentica.Output>;
}

export namespace Autentica {
  export type Params = {
    id_correios: string;
    token_api: string;
  };
  export type Output = {
    ambiente: string;
    id: string;
    ip: string;
    perfil: string;
    cnpj: string;
    emissao: Date;
    expiraEm: Date;
    zoneOffset: string;
    token: string;
  };
}

export interface AutenticaCartao {
  autenticaCartao: (
    input: AutenticaCartao.Params,
  ) => Promise<AutenticaCartao.Output>;
}

export namespace AutenticaCartao {
  export type Params = Autentica.Params & { numero_cartao: string };
  export type Output = Autentica.Output & {
    cartaoPostagem: {
      numero: string;
      contrato: string;
      dr: number;
      api: number[];
    };
  };
}

export interface AutenticaContrato {
  autenticaContrato: (
    input: AutenticaContrato.Params,
  ) => Promise<AutenticaContrato.Output>;
}

export namespace AutenticaContrato {
  export type Params = Autentica.Params & { numero_contrato: string };
  export type Output = Autentica.Output & {
    contrato: {
      numero: string;
      dr: number;
      api: number[];
    };
  };
}
