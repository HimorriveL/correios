export interface Postada {
  solicitaPostada: (input: Postada.Params) => Promise<Postada.Output>;
}

export namespace Postada {
  export type Params = {
    codigoObjeto?: string;
    dataPostagem?: number;
    page?: number;
    size?: number;
    tokenContrato: string;
  };
  export type Output = {
    itens: {
      cepDestino: string;
      valorAtendimento: number;
      codigoObjeto: string;
      codigoServico: string;
      nomeServico: string;
      alturaObjeto: string;
      comprimentoObjeto: string;
      larguraObjeto: string;
      pesoObjeto: string;
      pesoTarifadoObjeto: string;
      dataPostagem: string;
    }[];
    page: {
      size: number;
      numberElements: number;
      totalPages: number;
      number: number;
      count: number;
      next: boolean;
      previous: boolean;
      first: boolean;
      last: boolean;
    };
  };
}
