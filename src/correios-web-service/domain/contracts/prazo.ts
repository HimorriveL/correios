export interface Prazo {
  prazo: (input: Prazo.Params) => Promise<Prazo.Output>;
}

export namespace Prazo {
  export type Params = {
    tokenContrato: string;
    coProduto: string;
    cepOrigem: string;
    cepDestino: string;
  };
  export type Output = {
    coProduto: string;
    prazoEntrega: number;
    dataMaxima: Date;
    entregaDomiciliar: 'S' | 'N';
    entregaSabado: 'S' | 'N';
  };
}

export interface PrazoLista {
  prazoLista: (input: PrazoLista.Params) => Promise<PrazoLista.Output>;
}

export namespace PrazoLista {
  type ParametrosPrazo = {
    cepDestino: string;
    cepOrigem: string;
    coProduto: string;
    nuRequisicao: string;
    dtEvento: string;
  };
  export type Params = {
    tokenContrato: string;
    parametrosPrazo: ParametrosPrazo[];
  };
  export type Output = {
    coProduto: string;
    prazoEntrega: number;
    dataMaxima: Date;
    entregaDomiciliar: 'S' | 'N';
    entregaSabado: 'S' | 'N';
  }[];
}
