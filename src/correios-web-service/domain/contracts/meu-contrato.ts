export interface ListaServicos {
  listaServicos: (input: ListaServicos.Params) => Promise<ListaServicos.Output>;
}

export namespace ListaServicos {
  export type Params = {
    cnpj: string;
    contrato: string;
    cartao: string;
    tokenContrato: string;
  };
  export type Output = {
    codigo: string;
    descricao: string;
    coSegmento: string;
    descSegmento: string;
  }[];
}

export interface ConsultaCartao {
  consultaCartao: (
    input: ConsultaCartao.Params,
  ) => Promise<ConsultaCartao.Output>;
}

export namespace ConsultaCartao {
  export type Params = ListaServicos.Params;
  export type Output = {
    cnpj: string;
    nuContrato: string;
    nuSe: number;
    nuCartaoPostagem: string;
    cnpjCartao: string;
    dtInicioVigencia: string;
    dtFimVigencia: string;
    status: string;
  };
}
