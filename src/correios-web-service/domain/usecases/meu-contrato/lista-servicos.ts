import { AxiosHttpClient } from '../../../../infra/gateways';
import { ListaServicos } from '../../contracts';

type Params = ListaServicos.Params;
type Output = ListaServicos.Output;

export class SolicitaListaServicos implements ListaServicos {
  constructor(
    private readonly httpClient: AxiosHttpClient,
    private readonly url: string,
  ) {}

  async listaServicos({
    cartao,
    contrato,
    cnpj,
    tokenContrato,
  }: Params): Promise<Output> {
    const url = `${
      this.url
    }/${cnpj}/contratos/${contrato}/cartoes/${cartao.padStart(
      10,
      '0',
    )}/servicos?page=0&size=1000`;

    const values = await this.httpClient.request({
      method: 'GET',
      url,
      headers: { Authorization: `Bearer ${tokenContrato}` },
    });

    return values.itens;
  }
}
