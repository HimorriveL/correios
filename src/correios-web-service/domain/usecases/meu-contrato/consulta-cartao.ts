import { AxiosHttpClient } from '../../../../infra/gateways';
import { ConsultaCartao } from '../../contracts';

type Params = ConsultaCartao.Params;
type Output = ConsultaCartao.Output;

export class SolicitaConsultaCartao implements ConsultaCartao {
  constructor(
    private readonly httpClient: AxiosHttpClient,
    private readonly url: string,
  ) {}

  async consultaCartao({
    cartao,
    contrato,
    cnpj,
    tokenContrato,
  }: Params): Promise<Output> {
    const url = `${
      this.url
    }/${cnpj}/contratos/${contrato}/cartoes/${cartao.padStart(10, '0')}`;

    const values = await this.httpClient.request({
      method: 'GET',
      url,
      headers: { Authorization: `Bearer ${tokenContrato}` },
    });

    return values;
  }
}
