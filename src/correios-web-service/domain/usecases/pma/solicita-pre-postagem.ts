import { AxiosHttpClient } from '../../../../infra/gateways';
import { PrePostagem } from '../../contracts';

type Params = PrePostagem.Params;
type Output = PrePostagem.Output;

export class SolicitaPrePostagem implements PrePostagem {
  constructor(
    private readonly httpClient: AxiosHttpClient,
    private readonly url: string,
  ) {}

  async solicitaPrePostagem({
    body,
    tokenCartao,
    idCorreios,
    numeroCartaoPostagem,
  }: Params): Promise<Output> {
    const values = await this.httpClient.request({
      method: 'POST',
      url: this.url,
      headers: { Authorization: `Bearer ${tokenCartao}` },
      body,
      query: {
        idCorreios,
        numeroCartaoPostagem: numeroCartaoPostagem.padStart(10, '0'),
      },
    });

    return values;
  }
}
