import * as FormData from 'form-data';
import { AxiosHttpClient } from '../../../../infra/gateways';
import { PrePostagemLista } from '../../contracts';

type Params = PrePostagemLista.Params;
type Output = PrePostagemLista.Output;

export class SolicitaPrePostagemLista implements PrePostagemLista {
  constructor(
    private readonly httpClient: AxiosHttpClient,
    private readonly url: string,
  ) {}

  async solicitaPrePostagemLista({
    listaPostagem,
    tokenCartao,
    idCorreios,
    numeroCartaoPostagem,
  }: Params): Promise<Output> {
    const buffer = Buffer.from(JSON.stringify(listaPostagem), 'utf-8');
    const form = new FormData();
    form.append('arquivo', buffer, 'file.json');

    const values = await this.httpClient.request({
      method: 'POST',
      url: this.url,
      headers: {
        Authorization: `Bearer ${tokenCartao}`,
        'content-type': 'multipart/form-data',
      },

      body: form,
      query: {
        numeroCartaoPostagem: numeroCartaoPostagem.padStart(10, '0'),
        idCorreios,
      },
    });

    return values;
  }
}
