import { AxiosHttpClient } from '../../../../infra/gateways';
import { RangeEtiqueta } from '../../contracts';

type Params = RangeEtiqueta.Params;
type Output = RangeEtiqueta.Output;

export class SolicitaRangeEtiqueta implements RangeEtiqueta {
  constructor(
    private readonly httpClient: AxiosHttpClient,
    private readonly url: string,
  ) {}

  async solicitaRangeEtiqueta({
    codigoServico,
    quantidade,
    tokenCartao,
  }: Params): Promise<Output> {
    const values = await this.httpClient.request({
      method: 'POST',
      url: this.url,
      headers: { Authorization: `Bearer ${tokenCartao}` },
      body: { codigoServico, quantidade },
    });

    return values;
  }
}
