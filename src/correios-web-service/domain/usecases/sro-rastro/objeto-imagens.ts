import { AxiosHttpClient } from '../../../../infra/gateways';
import { ObjetoImagens } from '../../contracts';

type Params = ObjetoImagens.Params;
type Output = ObjetoImagens.Output;

export class SolicitaObjetoImagens implements ObjetoImagens {
  constructor(
    private readonly httpClient: AxiosHttpClient,
    private readonly url: string,
  ) {}

  async objetoImagens({ listaEtiqueta, tokenCartao }: Params): Promise<Output> {
    return this.httpClient.request({
      method: 'POST',
      url: this.url,
      body: listaEtiqueta,
      headers: { Authorization: `Bearer ${tokenCartao}` },
    });
  }
}
