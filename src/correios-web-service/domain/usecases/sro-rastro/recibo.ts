import { AxiosHttpClient } from '../../../../infra/gateways';
import { Recibo } from '../../contracts';

type Params = Recibo.Params;
type Output = Recibo.Output;

export class SolicitaRecibo implements Recibo {
  constructor(
    private readonly httpClient: AxiosHttpClient,
    private readonly url: string,
  ) {}

  async recibo({ recibo, tokenContrato }: Params): Promise<Output> {
    return this.httpClient.request({
      method: 'GET',
      url: `${this.url}/${recibo}`,
      headers: { Authorization: `Bearer ${tokenContrato}` },
    });
  }
}
