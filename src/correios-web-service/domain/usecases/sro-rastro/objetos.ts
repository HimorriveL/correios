import { AxiosHttpClient } from '../../../../infra/gateways';
import { Objetos } from '../../contracts';

type Params = Objetos.Params;
type Output = Objetos.Output;

export class SolicitaObjetos implements Objetos {
  constructor(
    private readonly httpClient: AxiosHttpClient,
    private readonly url: string,
  ) {}

  async objetos({
    codigosObjetos,
    resultado,
    tokenContrato,
  }: Params): Promise<Output> {
    let query = new URLSearchParams();
    codigosObjetos.forEach(objeto => {
      query.append('codigosObjetos', objeto);
    });
    query.append('resultado', resultado ?? 'T');
    return this.httpClient.request({
      method: 'GET',
      url: this.url,
      query,
      headers: { Authorization: `Bearer ${tokenContrato}` },
    });
  }
}
