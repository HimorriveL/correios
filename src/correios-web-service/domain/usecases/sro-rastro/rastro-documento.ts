import { AxiosHttpClient } from '../../../../infra/gateways';
import { RastrosDocumento } from '../../contracts';

type Params = RastrosDocumento.Params;
type Output = RastrosDocumento.Output;

export class SolicitaRastrosDocumento implements RastrosDocumento {
  constructor(
    private readonly httpClient: AxiosHttpClient,
    private readonly url: string,
  ) {}

  async rastrosDocumento({
    celular,
    destinatarioRemetente,
    documento,
    email,
    resultado,
    tokenContrato,
  }: Params): Promise<Output> {
    return this.httpClient.request({
      method: 'GET',
      url: this.url,
      query: {
        documento,
        email,
        celular,
        resultado: resultado ?? 'T',
        destinatarioRemetente: destinatarioRemetente ?? 'A',
      },
      headers: { Authorization: `Bearer ${tokenContrato}` },
    });
  }
}
