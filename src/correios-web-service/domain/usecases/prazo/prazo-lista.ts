import { AxiosHttpClient } from '../../../../infra/gateways';
import { PrazoLista } from '../../contracts';

type Params = PrazoLista.Params;
type Output = PrazoLista.Output;

export class SolicitaPrazoLista implements PrazoLista {
  constructor(
    private readonly httpClient: AxiosHttpClient,
    private readonly url: string,
  ) {}
  prazo: (input: PrazoLista.Params) => Promise<PrazoLista.Output>;

  async prazoLista({
    parametrosPrazo,
    tokenContrato,
  }: Params): Promise<Output> {
    return this.httpClient.request({
      method: 'POST',
      url: this.url,
      body: {
        idLote: '1',
        parametrosPrazo,
      },
      headers: { Authorization: `Bearer ${tokenContrato}` },
    });
  }
}
