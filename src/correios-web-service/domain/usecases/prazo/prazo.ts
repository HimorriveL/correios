import { AxiosHttpClient } from '../../../../infra/gateways';
import { Prazo } from '../../contracts';

type Params = Prazo.Params;
type Output = Prazo.Output;

export class SolicitaPrazo implements Prazo {
  constructor(
    private readonly httpClient: AxiosHttpClient,
    private readonly url: string,
  ) {}

  async prazo({
    cepDestino,
    cepOrigem,
    coProduto,
    tokenContrato,
  }: Params): Promise<Output> {
    const url = `${this.url}/${coProduto}`;
    return this.httpClient.request({
      method: 'GET',
      url,
      query: {
        cepOrigem,
        cepDestino,
      },
      headers: { Authorization: `Bearer ${tokenContrato}` },
    });
  }
}
