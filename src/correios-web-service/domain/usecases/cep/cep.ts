import { AxiosHttpClient } from '../../../../infra/gateways';
import { Cep } from '../../contracts';

type Params = Cep.Params;
type Output = Cep.Output;

export class SolicitaCep implements Cep {
  constructor(
    private readonly httpClient: AxiosHttpClient,
    private readonly url: string,
  ) {}

  async cep({ cep, tokenContrato }: Params): Promise<Output> {
    const url = `${this.url}/${cep}`;
    return this.httpClient.request({
      method: 'GET',
      url,
      headers: { Authorization: `Bearer ${tokenContrato}` },
    });
  }
}
