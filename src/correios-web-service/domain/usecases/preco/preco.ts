import { AxiosHttpClient } from '../../../../infra/gateways';
import { Preco } from '../../contracts';

type Params = Preco.Params;
type Output = Preco.Output;

export class SolicitaPreco implements Preco {
  constructor(
    private readonly httpClient: AxiosHttpClient,
    private readonly url: string,
  ) {}

  async preco({
    cepDestino,
    cepOrigem,
    altura,
    comprimento,
    largura,
    psObjeto,
    servicosAdicionais,
    tpObjeto,
    vlDeclarado,
    tokenContrato,
    coProduto,
  }: Params): Promise<Output> {
    const url = `${this.url}/${coProduto}`;
    return this.httpClient.request({
      method: 'GET',
      url,
      query: {
        cepDestino,
        cepOrigem,
        altura,
        comprimento,
        largura,
        psObjeto,
        servicosAdicionais,
        tpObjeto,
        vlDeclarado,
      },
      headers: { Authorization: `Bearer ${tokenContrato}` },
    });
  }
}
