import { AxiosHttpClient } from '../../../../infra/gateways';
import { PrecoLista } from '../../contracts';

type Params = PrecoLista.Params;
type Output = PrecoLista.Output;

export class SolicitaPrecoLista implements PrecoLista {
  constructor(
    private readonly httpClient: AxiosHttpClient,
    private readonly url: string,
  ) {}
  Preco: (input: PrecoLista.Params) => Promise<PrecoLista.Output>;

  async precoLista({
    parametrosProduto,
    tokenContrato,
  }: Params): Promise<Output> {
    return this.httpClient.request({
      method: 'POST',
      url: this.url,
      body: {
        idLote: '1',
        parametrosProduto,
      },
      headers: { Authorization: `Bearer ${tokenContrato}` },
    });
  }
}
