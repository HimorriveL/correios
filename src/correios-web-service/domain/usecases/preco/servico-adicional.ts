import { AxiosHttpClient } from '../../../../infra/gateways';
import { ServicosAdicionais } from '../../contracts';

type Params = ServicosAdicionais.Params;
type Output = ServicosAdicionais.Output;

export class SolicitaServicoAdicional implements ServicosAdicionais {
  constructor(
    private readonly httpClient: AxiosHttpClient,
    private readonly url: string,
  ) {}

  async servicosAdicionais({
    tokenContrato,
    coProduto,
  }: Params): Promise<Output> {
    const url = `${this.url}/${coProduto.padStart(5, '0')}`;
    return this.httpClient.request({
      method: 'GET',
      url,
      headers: { Authorization: `Bearer ${tokenContrato}` },
    });
  }
}
