import { AxiosHttpClient } from '../../../../infra/gateways';
import { Autentica } from '../../contracts';

type Params = Autentica.Params;
type Output = Autentica.Output;

export class AutenticaToken implements Autentica {
  constructor(
    private readonly httpClient: AxiosHttpClient,
    private readonly url: string,
  ) {}

  async autentica({ id_correios, token_api }: Params): Promise<Output> {
    return this.httpClient.request({
      method: 'POST',
      url: this.url,
      auth: {
        username: id_correios,
        password: token_api,
      },
    });
  }
}
