import { AxiosHttpClient } from '../../../../infra/gateways';
import { AutenticaContrato } from '../../contracts';

type Params = AutenticaContrato.Params;
type Output = AutenticaContrato.Output;

export class AutenticaContratoToken implements AutenticaContrato {
  constructor(
    private readonly httpClient: AxiosHttpClient,
    private readonly url: string,
  ) {}

  async autenticaContrato({
    id_correios,
    token_api,
    numero_contrato,
  }: Params): Promise<Output> {
    return this.httpClient.request({
      method: 'POST',
      url: this.url,
      auth: {
        username: id_correios,
        password: token_api,
      },
      body: {
        numero: numero_contrato,
      },
    });
  }
}
