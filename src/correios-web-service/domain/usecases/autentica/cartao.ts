import { AxiosHttpClient } from '../../../../infra/gateways';
import { AutenticaCartao } from '../../contracts';

type Params = AutenticaCartao.Params;
type Output = AutenticaCartao.Output;

export class AutenticaCartaoToken implements AutenticaCartao {
  constructor(
    private readonly httpClient: AxiosHttpClient,
    private readonly url: string,
  ) {}

  async autenticaCartao({
    id_correios,
    token_api,
    numero_cartao,
  }: Params): Promise<Output> {
    return this.httpClient.request({
      method: 'POST',
      url: this.url,
      auth: {
        username: id_correios,
        password: token_api,
      },
      body: {
        numero: numero_cartao.padStart(10, '0'),
      },
    });
  }
}
