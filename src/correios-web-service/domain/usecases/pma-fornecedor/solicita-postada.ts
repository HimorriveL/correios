import * as FormData from 'form-data';
import { AxiosHttpClient } from '../../../../infra/gateways';
import { Postada } from '../../contracts';

type Params = Postada.Params;
type Output = Postada.Output;

export class SolicitaPostada implements Postada {
  constructor(
    private readonly httpClient: AxiosHttpClient,
    private readonly url: string,
  ) {}

  async solicitaPostada(params: Params): Promise<Output> {
    const { tokenContrato, ...rest } = params;

    const values = await this.httpClient.request({
      method: 'GET',
      url: this.url,
      headers: {
        Authorization: `Bearer ${tokenContrato}`,
      },
      query: rest,
    });

    return values;
  }
}
