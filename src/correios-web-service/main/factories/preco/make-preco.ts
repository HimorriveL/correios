import { makeAxiosHttpClient } from '../../../../main/factories/infra/gateways';
import { SolicitaPreco } from '../../../domain/usecases/preco';
import { Preco } from '../../../domain/contracts';

import { makeApiCwsPreco } from './url';

export const makePreco = (): Preco => {
  return new SolicitaPreco(makeAxiosHttpClient(), makeApiCwsPreco());
};
