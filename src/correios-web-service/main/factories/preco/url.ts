import { makeApiCws } from '../makeApiUrl';

export const makeApiCwsPrecoUrl = (): string => makeApiCws('/preco/v1');

export const makeApiCwsPreco = (): string => `${makeApiCwsPrecoUrl()}/nacional`;

export const makeApiCwsServicoAdicional = (): string =>
  `${makeApiCwsPrecoUrl()}/servicos-adicionais`;
