import { makeAxiosHttpClient } from '../../../../main/factories/infra/gateways';
import { SolicitaServicoAdicional } from '../../../domain/usecases/preco';
import { ServicosAdicionais } from '../../../domain/contracts';

import { makeApiCwsServicoAdicional } from './url';

export const makeServicoAdicional = (): ServicosAdicionais => {
  return new SolicitaServicoAdicional(
    makeAxiosHttpClient(),
    makeApiCwsServicoAdicional(),
  );
};
