import { makeAxiosHttpClient } from '../../../../main/factories/infra/gateways';
import { SolicitaPrecoLista } from '../../../domain/usecases/preco';
import { PrecoLista } from '../../../domain/contracts';

import { makeApiCwsPreco } from './url';

export const makePrecoLista = (): PrecoLista => {
  return new SolicitaPrecoLista(makeAxiosHttpClient(), makeApiCwsPreco());
};
