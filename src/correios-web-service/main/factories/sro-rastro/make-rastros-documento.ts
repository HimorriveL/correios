import { makeAxiosHttpClient } from '../../../../main/factories/infra/gateways';
import { RastrosDocumento } from '../../../domain/contracts';

import { makeApiCwsRastrosDocumento } from './url';
import { SolicitaRastrosDocumento } from '../../../domain/usecases/sro-rastro';

export const makeRastrosDocumentos = (): RastrosDocumento => {
  return new SolicitaRastrosDocumento(
    makeAxiosHttpClient(),
    makeApiCwsRastrosDocumento(),
  );
};
