import { makeAxiosHttpClient } from '../../../../main/factories/infra/gateways';
import { ObjetoImagens } from '../../../domain/contracts';

import { makeApiCwsImagens } from './url';
import { SolicitaObjetoImagens } from '../../../domain/usecases/sro-rastro';

export const makeObjetosImagem = (): ObjetoImagens => {
  return new SolicitaObjetoImagens(makeAxiosHttpClient(), makeApiCwsImagens());
};
