import { makeAxiosHttpClient } from '../../../../main/factories/infra/gateways';
import { Objetos } from '../../../domain/contracts';

import { makeApiCwsObjetos } from './url';
import { SolicitaObjetos } from '../../../domain/usecases/sro-rastro';

export const makeObjetos = (): Objetos => {
  return new SolicitaObjetos(makeAxiosHttpClient(), makeApiCwsObjetos());
};
