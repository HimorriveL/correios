import { makeApiCws } from '../makeApiUrl';

const makeApiCwsSroRastro = (path: string): string =>
  makeApiCws(`/srorastro${path}`);

export const makeApiCwsObjetos = (): string =>
  makeApiCwsSroRastro('/v1/objetos');

export const makeApiCwsRastrosDocumento = (): string =>
  makeApiCwsSroRastro('/v1/objetos/rastrosdocumento');

export const makeApiCwsImagens = (): string =>
  makeApiCwsSroRastro('/v1/objetos/imagens');

export const makeApiCwsRecibo = (): string => makeApiCwsSroRastro('/v1/recibo');
