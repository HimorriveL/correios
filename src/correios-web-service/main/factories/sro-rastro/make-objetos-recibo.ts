import { makeAxiosHttpClient } from '../../../../main/factories/infra/gateways';
import { Recibo } from '../../../domain/contracts';

import { makeApiCwsRecibo } from './url';
import { SolicitaRecibo } from '../../../domain/usecases/sro-rastro';

export const makeRecibo = (): Recibo => {
  return new SolicitaRecibo(makeAxiosHttpClient(), makeApiCwsRecibo());
};
