import { makeApiCws } from '../makeApiUrl';

export const makeApiCwsMeuContrato = (): string =>
  makeApiCws('/meucontrato/v1/empresas');
