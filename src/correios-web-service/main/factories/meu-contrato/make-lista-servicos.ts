import { SolicitaListaServicos } from '../../../domain/usecases/meu-contrato';
import { makeAxiosHttpClient } from '../../../../main/factories/infra/gateways';
import { ListaServicos } from '../../../domain/contracts';

import { makeApiCwsMeuContrato } from './url';

export const makeSolicitaListaServicos = (): ListaServicos => {
  return new SolicitaListaServicos(
    makeAxiosHttpClient(),
    makeApiCwsMeuContrato(),
  );
};
