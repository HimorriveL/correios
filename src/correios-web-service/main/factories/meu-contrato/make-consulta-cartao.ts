import { SolicitaConsultaCartao } from '../../../domain/usecases/meu-contrato';
import { makeAxiosHttpClient } from '../../../../main/factories/infra/gateways';
import { ConsultaCartao } from '../../../domain/contracts';

import { makeApiCwsMeuContrato } from './url';

export const makeSolicitaConsultaCartao = (): ConsultaCartao => {
  return new SolicitaConsultaCartao(
    makeAxiosHttpClient(),
    makeApiCwsMeuContrato(),
  );
};
