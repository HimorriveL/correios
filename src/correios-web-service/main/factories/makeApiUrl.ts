export const makeApiCws = (path: string): string =>
  `https://api.correios.com.br${path}`;
