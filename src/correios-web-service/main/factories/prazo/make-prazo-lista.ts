import { makeAxiosHttpClient } from '../../../../main/factories/infra/gateways';
import { SolicitaPrazoLista } from '../../../domain/usecases/prazo';
import { PrazoLista } from '../../../domain/contracts';

import { makeApiCwsPrazo } from './url';

export const makePrazoLista = (): PrazoLista => {
  return new SolicitaPrazoLista(makeAxiosHttpClient(), makeApiCwsPrazo());
};
