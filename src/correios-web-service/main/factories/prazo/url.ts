import { makeApiCws } from '../makeApiUrl';

export const makeApiCwsPrazo = (): string => makeApiCws('/prazo/v1/nacional');
