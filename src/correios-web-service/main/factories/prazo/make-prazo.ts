import { makeAxiosHttpClient } from '../../../../main/factories/infra/gateways';
import { SolicitaPrazo } from '../../../domain/usecases/prazo';
import { Prazo } from '../../../domain/contracts';

import { makeApiCwsPrazo } from './url';

export const makePrazo = (): Prazo => {
  return new SolicitaPrazo(makeAxiosHttpClient(), makeApiCwsPrazo());
};
