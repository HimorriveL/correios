import { SolicitaRangeEtiqueta } from '../../../domain/usecases/pma';
import { makeAxiosHttpClient } from '../../../../main/factories/infra/gateways';
import { RangeEtiqueta } from '../../../domain/contracts';
import { makeApiCwsPmaSolicitaRangeEtiqueta } from './url';

export const makeSolicitaRangeEtiqueta = (): RangeEtiqueta => {
  return new SolicitaRangeEtiqueta(
    makeAxiosHttpClient(),
    makeApiCwsPmaSolicitaRangeEtiqueta(),
  );
};
