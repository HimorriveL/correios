import { makeApiCws } from '../makeApiUrl';

export const makeApiCwsPma = (path: string): string =>
  makeApiCws(`/prepostagem/v1/prepostagens${path}`);

export const makeApiCwsPmaSolicitaRangeEtiqueta = (): string =>
  makeApiCwsPma('/rotulo/range');

export const makeApiCwsPmaSolicitaPrePostagemLista = (): string =>
  makeApiCwsPma('/lista/objetosregistrados');
