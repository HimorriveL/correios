import { SolicitaPrePostagemLista } from '../../../domain/usecases/pma';
import { makeAxiosHttpClient } from '../../../../main/factories/infra/gateways';
import { PrePostagemLista } from '../../../domain/contracts';
import { makeApiCwsPmaSolicitaPrePostagemLista } from './url';

export const makeSolicitaPrePostagemLista = (): PrePostagemLista => {
  return new SolicitaPrePostagemLista(
    makeAxiosHttpClient(),
    makeApiCwsPmaSolicitaPrePostagemLista(),
  );
};
