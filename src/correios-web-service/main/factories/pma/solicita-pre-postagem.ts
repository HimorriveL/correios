import { SolicitaPrePostagem } from '../../../domain/usecases/pma';
import { makeAxiosHttpClient } from '../../../../main/factories/infra/gateways';
import { PrePostagem } from '../../../domain/contracts';
import { makeApiCwsPma } from './url';

export const makeSolicitaPrePostagem = (): PrePostagem => {
  return new SolicitaPrePostagem(makeAxiosHttpClient(), makeApiCwsPma(''));
};
