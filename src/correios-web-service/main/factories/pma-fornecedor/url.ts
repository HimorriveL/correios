import { makeApiCws } from '../makeApiUrl';

export const makeApiCwsPmaFornecedor = (path: string): string =>
  makeApiCws(`/prepostagemfornecedor/v1/prepostagens${path}`);

export const makeApiCwsPmaFornecedorPostada = (): string =>
  makeApiCwsPmaFornecedor('/postada');
