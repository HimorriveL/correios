import { Postada } from '../../../domain/contracts';
import { SolicitaPostada } from '../../../domain/usecases/pma-fornecedor';
import { makeAxiosHttpClient } from '../../../../main/factories/infra/gateways';
import { makeApiCwsPmaFornecedorPostada } from './url';

export const makeSolicitaPostada = (): Postada => {
  return new SolicitaPostada(
    makeAxiosHttpClient(),
    makeApiCwsPmaFornecedorPostada(),
  );
};
