import { makeApiCws } from '../makeApiUrl';

const makeApiCwsEndereco = (path: string): string => makeApiCws('/cep') + path;

export const makeApiCwsCep = (): string => makeApiCwsEndereco('/v2/enderecos');
