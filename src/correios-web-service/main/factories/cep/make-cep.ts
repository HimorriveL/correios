import { makeAxiosHttpClient } from '../../../../main/factories/infra/gateways';
import { SolicitaCep } from '../../../domain/usecases/cep';
import { Cep } from '../../../domain/contracts';

import { makeApiCwsCep } from './url';

export const makeSolicitaCep = (): Cep => {
  return new SolicitaCep(makeAxiosHttpClient(), makeApiCwsCep());
};
