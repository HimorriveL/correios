import { makeApiCws } from '../makeApiUrl';

const makeApiCwsAuth = (path: string): string =>
  makeApiCws(`/token/v1/autentica${path}`);

export const makeApiCwsAuthToken = (): string => makeApiCwsAuth('');
export const makeApiCwsAuthCartao = (): string =>
  makeApiCwsAuth('/cartaopostagem');
export const makeApiCwsAuthContrato = (): string => makeApiCwsAuth('/contrato');
