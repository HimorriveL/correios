import { makeAxiosHttpClient } from '../../../../main/factories/infra/gateways';
import { AutenticaToken } from '../../../domain/usecases/autentica';
import { Autentica } from '../../../domain/contracts/autentica';

import { makeApiCwsAuthToken } from './url';

export const makeAutentica = (): Autentica => {
  return new AutenticaToken(makeAxiosHttpClient(), makeApiCwsAuthToken());
};
