import { makeAxiosHttpClient } from '../../../../main/factories/infra/gateways';
import { AutenticaContratoToken } from '../../../domain/usecases/autentica';
import { AutenticaContrato } from '../../../domain/contracts/autentica';

import { makeApiCwsAuthContrato } from './url';

export const makeAutenticaContrato = (): AutenticaContrato => {
  return new AutenticaContratoToken(
    makeAxiosHttpClient(),
    makeApiCwsAuthContrato(),
  );
};
