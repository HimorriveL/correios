import { makeAxiosHttpClient } from '../../../../main/factories/infra/gateways';
import { AutenticaCartaoToken } from '../../../domain/usecases/autentica';
import { AutenticaCartao } from '../../../domain/contracts/autentica';

import { makeApiCwsAuthCartao } from './url';

export const makeAutenticaCartao = (): AutenticaCartao => {
  return new AutenticaCartaoToken(
    makeAxiosHttpClient(),
    makeApiCwsAuthCartao(),
  );
};
