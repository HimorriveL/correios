import {
  RangeEtiqueta,
  PrePostagem,
  PrePostagemLista,
} from '../domain/contracts';
import {
  makeSolicitaRangeEtiqueta,
  makeSolicitaPrePostagem,
  makeSolicitaPrePostagemLista,
} from '../main/factories/pma';

export async function solicitaRangeEtiqueta(
  params: RangeEtiqueta.Params,
): Promise<RangeEtiqueta.Output> {
  return makeSolicitaRangeEtiqueta().solicitaRangeEtiqueta(params);
}

export async function solicitaPrePostagem(
  params: PrePostagem.Params,
): Promise<PrePostagem.Output> {
  return makeSolicitaPrePostagem().solicitaPrePostagem(params);
}

export async function solicitaPrePostagemLista(
  params: PrePostagemLista.Params,
): Promise<PrePostagemLista.Output> {
  return makeSolicitaPrePostagemLista().solicitaPrePostagemLista(params);
}
