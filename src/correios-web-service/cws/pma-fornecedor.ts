import { Postada } from '../domain/contracts';
import { makeSolicitaPostada } from '../main/factories/pma-fornecedor';

export async function solicitaPostada(
  params: Postada.Params,
): Promise<Postada.Output> {
  return makeSolicitaPostada().solicitaPostada(params);
}
