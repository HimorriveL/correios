import {
  ObjetoImagens,
  Recibo,
  Objetos,
  RastrosDocumento,
} from '../domain/contracts';
import {
  makeObjetos,
  makeObjetosImagem,
  makeRastrosDocumentos,
  makeRecibo,
} from '../main/factories/sro-rastro';

export async function imagens(
  params: ObjetoImagens.Params,
): Promise<ObjetoImagens.Output> {
  return makeObjetosImagem().objetoImagens(params);
}

export async function recibo(params: Recibo.Params): Promise<Recibo.Output> {
  return makeRecibo().recibo(params);
}

export async function objetos(params: Objetos.Params): Promise<Objetos.Output> {
  return makeObjetos().objetos(params);
}

export async function rastrosDocumento(
  params: RastrosDocumento.Params,
): Promise<RastrosDocumento.Output> {
  return makeRastrosDocumentos().rastrosDocumento(params);
}
