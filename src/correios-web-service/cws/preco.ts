import { Preco, PrecoLista, ServicosAdicionais } from '../domain/contracts';
import {
  makePreco,
  makePrecoLista,
  makeServicoAdicional,
} from '../main/factories/preco';

export async function preco(params: Preco.Params): Promise<Preco.Output> {
  return makePreco().preco(params);
}

export async function precoLista(
  params: PrecoLista.Params,
): Promise<PrecoLista.Output> {
  return makePrecoLista().precoLista(params);
}

export async function servicosAdicionais(
  params: ServicosAdicionais.Params,
): Promise<ServicosAdicionais.Output> {
  return makeServicoAdicional().servicosAdicionais(params);
}
