import {
  ListaServicos,
  ConsultaCartao,
} from '../domain/contracts/meu-contrato';
import {
  makeSolicitaListaServicos,
  makeSolicitaConsultaCartao,
} from '../main/factories/meu-contrato';

export async function listaServicos(
  params: ListaServicos.Params,
): Promise<ListaServicos.Output> {
  return makeSolicitaListaServicos().listaServicos(params);
}

export async function consultaCartao(
  params: ConsultaCartao.Params,
): Promise<ConsultaCartao.Output> {
  return makeSolicitaConsultaCartao().consultaCartao(params);
}
