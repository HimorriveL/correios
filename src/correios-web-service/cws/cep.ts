import { Cep } from '../domain/contracts';
import { makeSolicitaCep } from '../main/factories/cep';

export async function cep(params: Cep.Params): Promise<Cep.Output> {
  return makeSolicitaCep().cep(params);
}
