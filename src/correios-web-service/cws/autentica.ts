import { parseISO } from 'date-fns';
import {
  Autentica,
  AutenticaCartao,
  AutenticaContrato,
} from '../domain/contracts';
import {
  makeAutentica,
  makeAutenticaCartao,
  makeAutenticaContrato,
} from '../main/factories/autentica';

export async function autentica(
  params: Autentica.Params,
): Promise<Autentica.Output> {
  const resp = await makeAutentica().autentica(params);
  return {
    ...resp,
    expiraEm: parseISO(String(resp.expiraEm)),
    emissao: parseISO(String(resp.emissao)),
  };
}

export async function autenticaCartao(
  params: AutenticaCartao.Params,
): Promise<AutenticaCartao.Output> {
  const resp = await makeAutenticaCartao().autenticaCartao(params);
  return {
    ...resp,
    expiraEm: parseISO(String(resp.expiraEm)),
    emissao: parseISO(String(resp.emissao)),
  };
}

export async function autenticaContrato(
  params: AutenticaContrato.Params,
): Promise<AutenticaContrato.Output> {
  const resp = await makeAutenticaContrato().autenticaContrato(params);
  return {
    ...resp,
    expiraEm: parseISO(String(resp.expiraEm)),
    emissao: parseISO(String(resp.emissao)),
  };
}
