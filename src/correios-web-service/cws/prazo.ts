import { parseISO } from 'date-fns';
import { Prazo, PrazoLista } from '../domain/contracts';
import { makePrazo, makePrazoLista } from '../main/factories/prazo';

export async function prazo(params: Prazo.Params): Promise<Prazo.Output> {
  const prazo = await makePrazo().prazo(params);
  return { ...prazo, dataMaxima: parseISO(String(prazo.dataMaxima)) };
}

export async function prazoLista(
  params: PrazoLista.Params,
): Promise<PrazoLista.Output> {
  const lista = await makePrazoLista().prazoLista(params);
  return lista.map(elem => ({
    ...elem,
    dataMaxima: parseISO(String(elem.dataMaxima)),
  }));
}
