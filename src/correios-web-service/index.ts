import * as autentica from './cws/autentica';
import * as prazo from './cws/prazo';
import * as preco from './cws/preco';
import * as cep from './cws/cep';
import * as sroRastro from './cws/sro-rastro';
import * as meuContrato from './cws/meu-contrato';
import * as pma from './cws/pma';
import * as pmaFornecedor from './cws/pma-fornecedor';

export {
  autentica,
  prazo,
  preco,
  cep,
  sroRastro,
  meuContrato,
  pma,
  pmaFornecedor,
};
