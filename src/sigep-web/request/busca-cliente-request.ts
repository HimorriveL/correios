import axios, { AxiosError } from 'axios';
import { parseStringPromise } from 'xml2js';
import {
  ICliente,
  IContratos,
  ICartoesPostagem,
  IServicos,
} from '../interfaces';
import { BuscaClienteDTO } from '../dto';
import { env } from '../../main/config/env';
import { sigepWebError } from '../../application/errors';
import { HttpResponse, success } from '../../application/helpers';

async function BuscaCliente({
  idContrato,
  idCartaoPostagem,
  usuario,
  senha,
}: BuscaClienteDTO): Promise<HttpResponse> {
  const xmls = `<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:cli="http://cliente.bean.master.sigep.bsb.correios.com.br/">
  <soapenv:Header/>
  <soapenv:Body>
     <cli:buscaCliente>
        <idContrato>${idContrato}</idContrato>
        <idCartaoPostagem>${`0000000000${idCartaoPostagem}`.slice(
          -10,
        )}</idCartaoPostagem>
        <usuario>${usuario}</usuario>
        <senha>${senha}</senha>
     </cli:buscaCliente>
  </soapenv:Body>
</soapenv:Envelope>`;

  try {
    const res = await axios.post(env.URL_SIGEP_WEB, xmls, {
      headers: { 'Content-Type': 'text/xml; charset=utf-8' },
    });

    const options = {
      explicitArray: false,
    };

    const retorno = await parseStringPromise(res.data, options);

    const ClienteERP: ICliente =
      retorno['soap:Envelope']['soap:Body']['ns2:buscaClienteResponse'].return;

    // se ter somente um contrato, vai dar problema, aqui ele arruma isso
    if (!Array.isArray(ClienteERP.contratos)) {
      const contrato: IContratos = ClienteERP.contratos;
      ClienteERP.contratos = [];
      ClienteERP.contratos.push(contrato);
    }

    // se ter somente um cartao, vai dar problema, aqui ele arruma isso
    ClienteERP.contratos.map(contrato => {
      if (!Array.isArray(contrato.cartoesPostagem)) {
        const cartaoPostagem: ICartoesPostagem = contrato.cartoesPostagem;
        const listaCartao: ICartoesPostagem[] = [{ ...cartaoPostagem }];
        contrato.cartoesPostagem = listaCartao;
        return {
          ...contrato,
        };
      }
      return { ...contrato };
    });

    // se ter somente um servico, vai dar problema, aqui ele arruma isso
    ClienteERP.contratos.map(contrato => {
      contrato.cartoesPostagem.map(cartao => {
        if (!Array.isArray(cartao.servicos)) {
          const servico: IServicos = cartao.servicos;
          const listaServico: IServicos[] = [{ ...servico }];
          cartao.servicos = listaServico;
          return {
            ...cartao,
          };
        }
        return { ...cartao };
      });
      return { ...contrato };
    });

    return success(ClienteERP);
  } catch (err) {
    const erro = await sigepWebError(err as AxiosError);
    return erro;
  }
}
export { BuscaCliente };
