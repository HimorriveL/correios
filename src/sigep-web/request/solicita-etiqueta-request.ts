import axios, { AxiosError } from 'axios';
import { parseStringPromise } from 'xml2js';
import { HttpResponse, success } from '../../application/helpers';
import { sigepWebError } from '../../application/errors';
import { env } from '../../main/config/env';
import { SolicitaEtiquetasDTO } from '../dto';

async function SolicitaEtiquetas({
  idServico,
  usuario,
  senha,
  cnpj,
  quantidade,
}: SolicitaEtiquetasDTO): Promise<HttpResponse> {
  const xmls = `<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:cli="http://cliente.bean.master.sigep.bsb.correios.com.br/">
  <soapenv:Header/>
  <soapenv:Body>
     <cli:solicitaEtiquetas>
        <tipoDestinatario>C</tipoDestinatario>
        <identificador>${cnpj}</identificador>
        <idServico>${idServico}</idServico>
        <qtdEtiquetas>${quantidade}</qtdEtiquetas>
        <usuario>${usuario}</usuario>
        <senha>${senha}</senha>
     </cli:solicitaEtiquetas>
  </soapenv:Body>
</soapenv:Envelope>`;

  try {
    const res = await axios.post(env.URL_SIGEP_WEB, xmls, {
      headers: { 'Content-Type': 'text/xml; charset=utf-8' },
    });

    const options = {
      explicitArray: false,
    };

    const teee = await parseStringPromise(res.data, options);
    return success(
      teee['soap:Envelope']['soap:Body']['ns2:solicitaEtiquetasResponse']
        .return,
    );
  } catch (err) {
    const erro = await sigepWebError(err as AxiosError);
    return erro;
  }
}
export { SolicitaEtiquetas };
