import axios, { AxiosError } from 'axios';
import { parseStringPromise } from 'xml2js';
import { ICorreiosLog, IObjetoPostal } from '../interfaces';
import { SolicitaXmlPlpDTO } from '../dto';
import { env } from '../../main/config/env';
import { sigepWebError } from '../../application/errors';
import { HttpResponse, success } from '../../application/helpers';

export async function SolicitaXmlPLP({
  idPlpMaster,
  usuario,
  senha,
}: SolicitaXmlPlpDTO): Promise<HttpResponse> {
  const xmls = `<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:cli="http://cliente.bean.master.sigep.bsb.correios.com.br/">
  <soapenv:Header/>
  <soapenv:Body>
     <cli:solicitaXmlPlp>
        <!--Optional:-->
        <idPlpMaster>${idPlpMaster}</idPlpMaster>
        <!--Optional:-->
        <usuario>${usuario}</usuario>
        <!--Optional:-->
        <senha>${senha}</senha>
     </cli:solicitaXmlPlp>
  </soapenv:Body>
</soapenv:Envelope>`;

  try {
    const res = await axios.post(env.URL_SIGEP_WEB, xmls, {
      headers: { 'Content-Type': 'text/xml; charset=utf-8' },
    });

    const options = {
      explicitArray: false,
    };

    const retorno = await parseStringPromise(res.data, options);

    const retorno2: any = await parseStringPromise(
      retorno['soap:Envelope']['soap:Body']['ns2:solicitaXmlPlpResponse']
        .return,
      options,
    );

    const retorno3: ICorreiosLog = retorno2.correioslog;

    if (!Array.isArray(retorno3.objeto_postal)) {
      const objeto_postail: IObjetoPostal = retorno3.objeto_postal;
      retorno3.objeto_postal = [];
      retorno3.objeto_postal.push(objeto_postail);
    }

    return success(retorno3);
  } catch (err) {
    const erro = await sigepWebError(err as AxiosError);
    return erro;
  }
}
