import axios, { AxiosError } from 'axios';
import { parseStringPromise } from 'xml2js';
import { HttpResponse, success } from '../../application/helpers';
import { sigepWebError } from '../../application/errors';
import { env } from '../../main/config/env';

async function ConsultaCEP(
  cep: string,
  usuario: string,
  senha: string,
): Promise<HttpResponse> {
  const xmls = `<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:cli="http://cliente.bean.master.sigep.bsb.correios.com.br/">
  <soapenv:Header/>
  <soapenv:Body>
     <cli:consultaCEP>
        <cep>${cep}</cep>
        <usuario>${usuario}</usuario>
        <senha>${senha}</senha>
     </cli:consultaCEP>
  </soapenv:Body>
</soapenv:Envelope>`;
  try {
    const res = await axios.post(env.URL_SIGEP_WEB, xmls, {
      headers: { 'Content-Type': 'text/xml; charset=utf-8' },
    });

    const options = {
      explicitArray: false,
    };

    const teee = await parseStringPromise(res.data, options);

    return success(
      teee['soap:Envelope']['soap:Body']['ns2:consultaCEPResponse'].return,
    );
  } catch (err) {
    const erro = await sigepWebError(err as AxiosError);
    return erro;
  }
}

export { ConsultaCEP };
