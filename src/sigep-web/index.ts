import {
  BuscaClienteDTO,
  BuscaServicoDTO,
  SolicitaEtiquetasDTO,
  SolicitaPlpDTO,
  SolicitaXmlPlpDTO,
} from './dto';
import { HimorrivelCorreiosError } from '../utils';
import {
  BuscaCliente,
  BuscaServicos,
  ConsultaCEP,
  SolicitaEtiquetas,
  SolicitaPLP,
  SolicitaXmlPLP,
} from './request';
import {
  ICep,
  ICliente,
  ICorreiosLog,
  IEtiquetaResponse,
  IServicos,
} from './interfaces';

export async function buscaCliente(dto: BuscaClienteDTO): Promise<ICliente> {
  const retorno = await BuscaCliente(dto);
  if (retorno.statusCode === 200) {
    return retorno.data;
  }
  throw new HimorrivelCorreiosError(retorno.data.error);
}

export async function buscaServico(dto: BuscaServicoDTO): Promise<IServicos[]> {
  const retorno = await BuscaServicos(dto);
  if (retorno.statusCode === 200) {
    return retorno.data;
  }
  throw new HimorrivelCorreiosError(retorno.data.error);
}

export async function consultaCEP(
  cep: string,
  usuario: string,
  senha: string,
): Promise<ICep> {
  const retorno = await ConsultaCEP(cep, usuario, senha);
  if (retorno.statusCode === 200) {
    return retorno.data;
  }
  throw new HimorrivelCorreiosError(retorno.data.error);
}

export async function solicitaEtiquetas(
  dto: SolicitaEtiquetasDTO,
): Promise<IEtiquetaResponse> {
  const retorno = await SolicitaEtiquetas(dto);

  if (retorno.statusCode === 200) {
    return {
      prefixo: retorno.data.substring(0, 2),
      etiqueta_inicial: Number(retorno.data.substring(2, 10)),
      etiqueta_final: Number(retorno.data.substring(16, 25)),
      sufixo: retorno.data.substring(11, 13),
    };
  }
  throw new HimorrivelCorreiosError(retorno.data.error);
}

export async function solicitaPLP(dto: SolicitaPlpDTO): Promise<ICorreiosLog> {
  const retorno = await SolicitaPLP(dto);
  if (retorno.statusCode === 200) {
    return retorno.data;
  }
  throw new HimorrivelCorreiosError(retorno.data.error);
}

export async function solicitaXmlPLP(
  dto: SolicitaXmlPlpDTO,
): Promise<ICorreiosLog> {
  const retorno = await SolicitaXmlPLP(dto);
  if (retorno.statusCode === 200) {
    return retorno.data;
  }
  throw new HimorrivelCorreiosError(retorno.data.error);
}
