export interface SolicitaPlpDTO {
  idPlpMaster: string;
  numEtiqueta: string;
  usuario: string;
  senha: string;
}
