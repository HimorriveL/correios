export interface BuscaServicoDTO {
  idContrato: string;
  idCartaoPostagem: string;
  usuario: string;
  senha: string;
}
