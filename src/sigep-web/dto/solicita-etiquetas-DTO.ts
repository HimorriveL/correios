export interface SolicitaEtiquetasDTO {
  idServico: string;
  usuario: string;
  senha: string;
  cnpj: string;
  quantidade: number;
}
