export interface BuscaClienteDTO {
  idContrato: string;
  idCartaoPostagem: string;
  usuario: string;
  senha: string;
}
