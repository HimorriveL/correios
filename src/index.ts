import * as sigepWeb from './sigep-web';
import * as cws from './correios-web-service';
import { HimorrivelCorreiosError } from './utils';

export { cws, sigepWeb, HimorrivelCorreiosError };
